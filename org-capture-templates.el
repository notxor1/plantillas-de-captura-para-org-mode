;; Capturar cualquier cosa a formato org-mode
(setq org-capture-templates
	      ; Es una lista de listas así que vamos por partes
      '(("t" "Tarea Pendiente")
	("tt" "Tarea Simple    (t) trabajo" entry (file "~/agenda/agenda.org")
	 "* PENDIENTE %? \t :trabajo:
   :PROPERTIES:
   :CREATE:      %U
   :END:
   :LOGBOOK:
   - State  \"PENDIENTE\"            from \"\"      %U
   :END:" :empty-lines 1)
	("ta" "Tarea Simple    (a) asociación" entry (file+headline "~/agenda/agenda.org" "Asociación")
	 "* PENDIENTE %? \t :pica:
   :PROPERTIES:
   :CREATE:       %U
   :END:
   :LOGBOOK:
   - State  \"PENDIENTE\"            from \"\"      %U
   :END:" :empty-lines 1)
	("tp" "Tarea Simple    (p) personal" entry (file+headline "~/agenda/agenda.org" "Personal")
	 "* PENDIENTE %? \t :personal:
   :PROPERTIES:
   :CREATE:       %U
   :END:
   :LOGBOOK:
   - State  \"PENDIENTE\"            from \"\"      %U
   :END:" :empty-lines 1)
 ;;; Capturar tareas que pasan a estar a la espera
	("e" "Tarea a la espera")
	("et" "Tarea Simple    (t) trabajo" entry (file "~/agenda/agenda.org")
	 "* ESPERANDO %? \t :trabajo:
   :PROPERTIES:
   :CREATE:      %U
   :END:
   :LOGBOOK:
   - State  \"ESPERANDO\"            from \"\"      %U
   :END:" :empty-lines 1)
	("ea" "Tarea Simple    (a) asociación" entry (file+headline "~/agenda/agenda.org" "Asociación")
	 "* ESPERANDO %? \t :pica:
   :PROPERTIES:
   :CREATE:       %U
   :END:
   :LOGBOOK:
   - State  \"ESPERANDO\"            from \"\"      %U
   :END:" :empty-lines 1)
	("ep" "Tarea Simple    (p) personal" entry (file+headline "~/agenda/agenda.org" "Personal")
	 "* ESPERANDO %? \t :personal:
   :PROPERTIES:
   :CREATE:       %U
   :END:
   :LOGBOOK:
   - State  \"ESPERANDO\"            from \"\"      %U
   :END:" :empty-lines 1)
 ;;; Capturas que tienen «deadline» asociada
	("l" "Tarea con fecha límite")
	("lt" "Tarea    (t) trabajo" entry (file "~/agenda/agenda.org")
	 "* PENDIENTE %? \t :trabajo:
   DEADLINE: %(substring (call-interactively 'org-deadline) 12)
   :PROPERTIES:
   :CREATE:      %U
   :END:
   :LOGBOOK:
   - State  \"PENDIENTE\"            from \"\"      %U
   :END:" :empty-lines 1)
	("la" "Tarea    (a) asociación" entry (file+headline "~/agenda/agenda.org" "Asociación")
	 "* PENDIENTE %? \t :pica:
   DEADLINE: %(substring (call-interactively 'org-deadline) 12)
   :PROPERTIES:
   :CREATE:       %U
   :END:
   :LOGBOOK:
   - State  \"PENDIENTE\"            from \"\"      %U
   :END:" :empty-lines 1)
	("lp" "Tarea    (p) personal" entry (file+headline "~/agenda/agenda.org" "Personal")
	 "* PENDIENTE %? \t :personal:
   DEADLINE: %(substring (call-interactively 'org-deadline) 12)
   :PROPERTIES:
   :CREATE:       %U
   :END:
   :LOGBOOK:
   - State  \"PENDIENTE\"            from \"\"      %U
   :END:" :empty-lines 1)
 ;;; Templates de captura para cosas que no son para la agenda
	("n" "Capturas no para agenda")
	("nc" "Anotar (c) contacto" entry (file+headline "~/agenda/especiales/personal.org.gpg" "Sin ordenar")
	 "** %^{Nombre} %^{Apellidos}%?
   :PROPERTIES:
   :Nombre:     %\\1
   :Apellidos:  %\\2
   :Alias:      %^{Alias}
   :Grupo:      %^{Grupo}
   :F-nacim:    %^{F-nacim}u
   :Móvil:      %^{Móvil}
   :Teléfono:
   :Email:      %^{Email}
   :Web:
   :Dirección:  %^{Dirección}
   :Ciudad:     %^{Ciudad}
   :Provincia:  %^{Provincia}
   :Cód.Pos:    %^{Código Postal}
   :Compañía:
   :Notas:
   :END:" :empty-lines 1)
	("nd" "Anotar (d) diario" entry (file+headline "~/agenda/bitacora.org" "Diario")
	 "** %U
%?" :empty-lines 1)
	("ni" "Anotar (i) idea" entry (file+headline "~/agenda/bitacora.org" "Ideas")
	 "** Idea para %^{Tema}
   :PROPERTIES:
   :Tema:      %\\1
   :fecha:     %U
   :END:
%?" :empty-lines 1)
    ("np" "Anotar (p) presupuesto" entry (file+headline "~/agenda/cuentas.org" "Presupuestos")
     "** Presupuesto para %^{Cliente}
   :PROPERTIES:
   :Cliente:       %\\1
   :fecha:         %U
   :END:
%T

| Concepto                              | Precio (€) | Cantidad  | Total (€) |
| <30>                                  | <9>        | <9>       | <9>       |
|---------------------------------------+------------+-----------+-----------|
| %?                                    |            |           |           |
|                                       |            |           |           |
|---------------------------------------+------------+-----------+-----------|
| Total                                 |            |           |           |
#+TBLFM: $4=$2*$3;%.2f::@>$4=vsum(@3..@-1);%.2f
" :empty-lines 1)
;;; Lista de templates de captura para Frateco
	("f" "Tareas para Frateco Esperanto")
	("ff" "Tarea    (f) Frateco" entry (file+headline "~/agenda/agenda.org" "Esperanto")
	 "* PENDIENTE %? \t :personal:
   DEADLINE: %(substring (call-interactively 'org-deadline) 12)
   :PROPERTIES:
   :CREATE:       %U
   :END:
   :LOGBOOK:
   - State  \"PENDIENTE\"            from \"\"      %U
   :END:" :empty-lines 1)
	))
